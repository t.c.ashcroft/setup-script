#!/bin/bash

DIR=$(pwd)

sudo apt update
sudo apt install -y i3-wm i3lock i3status i3blocks compton fonts-font-awesome unclutter

mkdir -p ~/Programs
cd ~/Programs

git clone https://github.com/glsorre/i3-gnome i3-gnome
cd i3-gnome
sudo make install

cd ~/.config/
rm -rf i3
git clone https://gitlab.com/t.c.ashcroft/i3-configs ./i3
mkdir -p ~/.config/i3blocks
ln -s ~/.config/i3/i3blocks ~/.config/i3blocks/config
cd -

sudo apt install -y rofi
mkdir -p ~/.config/rofi
mkdir -p ~/.config/rofi/themes
cp $DIR/config.rofi ~/.config/rofi/config
cp $DIR/arthur-mod.rasi ~/.config/rofi/themes/arthur-mod.rasi

