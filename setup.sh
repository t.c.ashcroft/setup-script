#!/bin/bash

# Setup Ubuntu 19.x System

ARG=$1

# Initial System Update
sudo apt update && sudo apt upgrade -y


#######################################
# General Tools

sudo apt install -y vim xclip htop arandr libreoffice remmina gnome-tweak-tool lynx nitrogen build-essential curl apt-transport-https
sudo snap install slack --classic 
sudo snap install spotify

mkdir ~/bin
mkdir ~/Programs

# Brave
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

source /etc/os-release

echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/brave-browser-release-${UBUNTU_CODENAME}.list

sudo apt update

sudo apt install brave-browser

# VIM
git clone --recurse-submodules https://gitlab.com/t.c.ashcroft/vim-config ~/.vim

DIR=$(pwd)
./setup-i3.sh
cd $DIR

#######################################
# Dev Tools
if [[ "work" == $ARG || "dev" == $ARG ]]
then 

  echo "Setting up dev tools"
  mkdir ~/workspace

  sudo apt install -y openjdk-11-jdk maven docker.io mongodb-clients  
  sudo groupadd docker
  sudo usermod -aG docker $USER
  sudo snap install postman

fi


#######################################
# Work Setup
if [[ "work" == $ARG ]]
then

  echo "Setting up work machine"

  sudo mkdir -p /srv/log
  sudo chown -R $USER:$USER /srv/log

  git clone https://gitlab.com/t.c.ashcroft/dot-files.git ~/.dotfiles
  bash ~/.dotfiles/install.sh  

  # Intellij
  # Note: This link will break with subsequent versions of intellij
  cd Downloads
  wget https://download.jetbrains.com/idea/ideaIU-2019.2.tar.gz
  mv ideaIU-2019.2.tar.gz ~/Programs
  cd ~/Programs
  mkdir intellij
  tar xf ideaIU-2019.2.tar.gz -C intellij --strip-components=1
  ln -s ~/Programs/intellij/bin/idea.sh ~/bin/intellij
  cd ~

  
  echo "Don't forget to get your SSH Key into our remote Git provider."

fi

gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"

ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

sudo apt autoremove -y

